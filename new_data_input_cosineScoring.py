# -*- coding: utf-8 -*-
"""
Created on Wed Aug 22 17:01:05 2018

@author: Kas Houthuijs
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
import itertools

np.seterr(divide='ignore', invalid='ignore')

# User input
# Convolution options
scale = 0.965  # Scaling factor
FWHM = 12  # cm-1, FWHM of convolution
granularity = 10  # granularity = 10 => interval has 10 sections
CONST = 0.89

# Folder and file options
current_dir = os.getcwd()
output_dir = current_dir
output_file = output_dir + "/outputFile.csv"  # output file (.csv)

# Define some variables
txt_list = []
txt_dict = {}
theory_data_dict = {}
scores = []

# Open experimental spectrum
experiment_file = "Experiment.txt"
experiment_data = pd.read_csv(experiment_file, sep="\t", header=None, names=["Wavenumber", "Intensity"])
experiment_data = experiment_data.round(5)
# experiment_data = experiment_data['Intensity'].round()

row1 = "wavenumber, " + experiment_file

experiment_data['Intensity'] = round(experiment_data['Intensity'] / experiment_data['Intensity'].max(), 10)


# Euclidean Distance
def euclidean_distance(x, y):
    return np.sqrt(sum(pow(a - b, 2) for a, b in zip(x, y)))


# Jaccard similarity
def jaccard_similarity(x, y):
    intersection_cardinality = len(set.intersection(*[set(x), set(y)]))
    union_cardinality = len(set.union(*[set(x), set(y)]))
    return intersection_cardinality / float(union_cardinality)


# Function that scores the match between experimental and computed spectrum
def cosine_scoring(exp_spectrum, comp_spectrum, c=0.9):
    txt_exp = np.log(np.asarray(exp_spectrum) + c)
    txt_comp = np.log(np.asarray(comp_spectrum) + c)
    score = np.dot(txt_exp, txt_comp) / np.sqrt(np.dot(txt_exp, txt_exp) * np.dot(txt_comp, txt_comp))
    return score


# Function that reads a Gaussian log file to construct an IR spectrum
#   corresponding to the frequencies
def calculate_gaussian(theory_data, experiment_wave, scale, FWHM=FWHM):
    sigma = FWHM / (2 * np.sqrt(2 * np.log(2)))

    theory_data['Wavenumber'] = theory_data['Wavenumber'] * scale
    theory_data = theory_data.round(5)

    if theory_data['Wavenumber'].min() < 0:
        print("Warning: imaginary frequencies found in %s!" % (txt_file))
    peaksum = np.zeros(shape=(len(experiment_wave), 1))
    # Scaling:
    for wavenumber_theory, intensity_theory in theory_data.values:
        for index, wavenumber_experiment in enumerate(experiment_wave, start=0):
            peaksum[index] += intensity_theory * np.exp(-np.square(float(wavenumber_experiment) - float(wavenumber_theory)) / (2 * sigma * sigma))
    ymax = peaksum.max()
    peaksum[:] = [y / ymax for y in peaksum]
    peaksum = np.transpose(peaksum).round(10).tolist()[0]
    return peaksum


def combine_x_theories(theory_data, experiment_wave, experiment_intensity, c=0.89):
    # Start with 2 theories, end with all theories
    for number_of_theories in range(2, len(theory_data)):
        # All possible combinations of x theories
        for theories in itertools.combinations(theory_data, number_of_theories):
            # CAUTION: possible_weights is potentially VERY HIGH: 3 theories with granularity 10 -> 756 combinations
            possible_weights = [weights for weights in itertools.permutations(set([n / granularity for n in range(1, granularity)] * 3), number_of_theories) if sum(weights) == 1]
            # Each theory gets it weight
            for fwhm in range(1, 20):
                highest_score = 0
                best_weights = [0] * len(theories)
                for weights in possible_weights:
                    # weight1  theory1 + weight2 * theory2 ..
                    combined_theory_data = theory_data[theories[0]]
                    for i in range(1, len(theories)):
                        combined_theory_data = combined_theory_data.append(theory_data[theories[i]], ignore_index=True)
                    combined_theory_data.sort_values(by=['Wavenumber'])
                    combined_fitted_intensity = calculate_gaussian(combined_theory_data, experiment_wave, scale, fwhm)
                    new_score = cosine_scoring(experiment_intensity, comp_spectrum=combined_fitted_intensity, c=c)
                    if new_score == 0 or not new_score:
                        breakpoint()
                    if new_score > highest_score:
                        highest_score = new_score
                        best_weights = weights

                print('fwhm: ', fwhm, 'theories: ', theories, 'highest_score: ', highest_score, ' best_weights: ', best_weights)


def combine_two_theories(theory_intesity_1, theory_intesity_2, experiment_intensity):
    score1 = cosine_scoring(experiment_intensity, comp_spectrum=theory_intesity_1)
    score2 = cosine_scoring(experiment_intensity, comp_spectrum=theory_intesity_2)
    highest_score = max(score1, score2)
    best_weight = np.nan
    # 1.Test: 0.01 * theory1 + 0.99 * theory2
    # 2.Test: 0.02 * theory1 + 0.98 * theory2
    for weight in range(1, 100):
        theory_intesity_12 = weight / 100 * theory_intesity_1 + (1 - weight / 100) * theory_intesity_2
        # Also usable for constant: c=0.01 => for c in range(100, 100)
        for c in range(1, 100):
            # c=0.01, c=0.02, ... c=1
            new_score = cosine_scoring(experiment_intensity, comp_spectrum=theory_intesity_12, c=(c / 100))
            if new_score > highest_score:
                highest_score = new_score
                best_weight = weight / 100
                best_c = c

    print('Best c: ', best_c / 100)
    best_combination = best_weight * theory_intesity_1 + (1 - best_weight) * theory_intesity_2
    return highest_score, best_weight, best_combination


# Open .txt files which start with Theory and extract their computed IR spectrum
for file in os.listdir(current_dir):
    if file.startswith("Theory") and file.endswith(".txt"):
        txt_list.append(file)

# Go through all found theory files
for txt_file in txt_list:
    # Read all theory data
    theory_data_dict[txt_file] = pd.read_csv(txt_file, sep="\t", header=None, names=["Wavenumber", "Intensity"])
    txt_dict[txt_file] = [calculate_gaussian(theory_data_dict[txt_file], experiment_data['Wavenumber'], scale=scale, FWHM=FWHM)]
    score = cosine_scoring(experiment_data['Intensity'], comp_spectrum=txt_dict[txt_file][0])
    scores.append((txt_file, score, txt_dict[txt_file][0]))

# Sort scores -> Highest score first
sorted_scores = sorted(scores, key=lambda tup: tup[1], reverse=True)

# New pd.Dataframe: combine experiment data with new calculated data
combined_data = experiment_data.copy(deep=True)
combined_data.columns = ['Wavenumber', 'Experiment']

# Write csv file header and add new containing the experimental and computed spectra, including their scores
# for txt_file, score in sorted_scores:
#     # Name column after file name (without .txt) and add score in braces
#     column_name = "%s (%.4f)" % (txt_file[:-4], round(score, 4))
#     row1 = row1 + ", " + column_name
#     combined_data[str(column_name)] = pd.Series(txt_dict[txt_file][0][:], index=combined_data.index)


# Open/create output file and add calculated data
# f = open(output_file, "w")
# f.write(row1 + "\n")
# for i in range(len(experiment_data['Intensity'])):
#     row = "%.5f, %.10f" % (experiment_data['Wavenumber'][i], experiment_data['Intensity'][i])
#     for txt_file, score in sorted_scores:
#         row = row + ", %.10f" % (txt_dict[txt_file][0][i])
#     f.write(row + "\n")

# f.close()

#

combine_x_theories(theory_data_dict, experiment_data['Wavenumber'], experiment_data['Intensity'], c=CONST)

# Try all possible combination of two different theories
# for a, b in itertools.combinations(columns, 2):
#     best_score, best_weight, best_combination = combine_two_theories(combined_data[a], combined_data[b], experiment_data['Intensity'])
#     combined_data["%s * %s + %s * %s = %s" % (best_weight, a, (1 - best_weight), b, best_score)] = pd.Series(best_combination, index=combined_data.index)
breakpoint()
# Plot experiment and calculated data
ax = plt.gca()
combined_data.plot(kind='line', x='Wavenumber', y=experiment_data['Intensity'], ax=ax)
for column in combined_data.columns[1 + len(txt_dict):]:
    combined_data.plot(kind='line', x='Wavenumber', y=column, ax=ax)

plt.show()
