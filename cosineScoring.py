# -*- coding: utf-8 -*-
"""
Created on Wed Aug 22 17:01:05 2018

@author: Gashi Bonaventura Stenzel
"""
import numpy as np
import pandas as pd
import os


# Euclidean Distance
def euclidean_distance(x, y):
    return np.sqrt(sum(pow(a - b, 2) for a, b in zip(x, y)))


# Jaccard similarity
def jaccard_similarity(x, y):
    intersection_cardinality = len(set.intersection(*[set(x), set(y)]))
    union_cardinality = len(set.union(*[set(x), set(y)]))
    return intersection_cardinality / float(union_cardinality)


# Function that scores the match between experimental and computed spectrum
def cosine_scoring(exp_spectrum, comp_spectrum):
    c = 0.9
    log_exp = np.log(np.asarray(exp_spectrum) + c)
    log_comp = np.log(np.asarray(comp_spectrum) + c)
    score = np.dot(log_exp, log_comp) / np.sqrt(np.dot(log_exp, log_exp) * np.dot(log_comp, log_comp))
    return score


# Function that reads a Gaussian log file to construct an IR spectrum
#   corresponding to the frequencies
def readlog_and_calculate_gaussian(file_name, experiment_wave, scale, sigma):
    theory_data = pd.read_csv(file_name, sep="\t", header=None, names=["Wavenumber", "Intensity"])
    if theory_data['Wavenumber'].min() < 0:
        print("Warning: imaginary frequencies found in %s!" % (log_file))
    peaksum = [0] * len(experiment_wave)
    # Scaling:
    theory_data['Wavenumber'] = theory_data['Wavenumber'] * scale
    for wavenumber_theory, intensity_theory in theory_data.values[:2]:
        for index, wavenumber_experiment in enumerate(experiment_wave[:3], start=0):
            peaksum[index] += intensity_theory * np.exp(-np.square(wavenumber_experiment - wavenumber_theory) / (2 * sigma * sigma))
            print('index', index)
            print('intensity_theory', intensity_theory)
            print('wavenumber_experiment', wavenumber_experiment)
            print('wavenumber_theory', wavenumber_theory)

    ymax = max(peaksum)
    peaksum[:] = [round(y / ymax) for y in peaksum]
    return peaksum


# User input
# Convolution options
scale = 0.965  # Scaling factor
FWHM = 12  # cm-1, FWHM of convolution
sigma = FWHM / (2 * np.sqrt(2 * np.log(2)))

# Folder and file options
current_dir = os.getcwd()
output_dir = current_dir
comparison_file = output_dir + "/outputFile.csv"  # output file (.csv)

# Define some variables
log_list = []
log_dict = {}
scores = []

# Open experimental spectrum
experiment_file = "Experiment.txt"
experiment_data = pd.read_csv(experiment_file, sep="\t", header=None, names=["Wavenumber", "Intensity"])
row1 = "wavenumber, " + experiment_file

experiment_data['Intensity'] = round(experiment_data['Intensity'] / experiment_data['Intensity'].max(), 10)

# Open txt files and extract their computed IR spectrum
for file in os.listdir(current_dir):
    if file.startswith("Theory-03") and file.endswith(".txt"):
        log_list.append(file)

for log_file in log_list:
    log_dict[log_file] = [readlog_and_calculate_gaussian(log_file, experiment_data['Wavenumber'], scale, sigma)]
    if not log_dict[log_file]:
        del log_dict[log_file]
        print(log_file + " errored")
    else:
        # If log file is ok, calculate matching score
        log_dict[log_file].append(cosine_scoring(experiment_data['Intensity'], log_dict[log_file][0]))
        scores.append((log_file, log_dict[log_file][1]))

sorted_scores = sorted(scores, key=lambda tup: tup[1], reverse=True)

# Write csv file containing the experimental and computed spectra, including their scores
for log_file, score in sorted_scores:
    row1 = row1 + ", %s (%.4f)" % (log_file[:-4], round(score, 4))
f = open(comparison_file, "w")
print(comparison_file)
f.write(row1 + "\n")

for i in range(len(experiment_data['Wavenumber'])):
    row = "%.5f, %.10f" % (experiment_data['Wavenumber'][i], experiment_data['Intensity'][i])
    for log_file, score in sorted_scores:
        row = row + ", %.10f" % (log_dict[log_file][0][i])
    f.write(row + "\n")

f.close()