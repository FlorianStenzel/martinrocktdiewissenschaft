# -*- coding: utf-8 -*-
"""
Created on Wed Aug 22 17:01:05 2018

@author: Kas Houthuijs
"""
import numpy as np
import os

#%% Function that scores the match between experimental and computed spectrum
def cosine_scoring(exp_spectrum,comp_spectrum):
    c = 0.9
    log_exp = np.log(np.asarray(exp_spectrum)+c)
    log_comp = np.log(np.asarray(comp_spectrum)+c)
    score = np.dot(log_exp,log_comp)/np.sqrt(np.dot(log_exp,log_exp)*np.dot(log_comp,log_comp))
    return score

#%% Function that reads a Gaussian log file to construct an IR spectrum
#   corresponding to the frequencies
def readlog(f,xaxis,scale,FWHM):
    sigma = FWHM/(2*np.sqrt(2*np.log(2)))
    peaksum = []
    frequencies = []
    intensities = []

    lines = f.read().splitlines()
    redflag = False
    for i in range(len(lines)):
        row = lines[i]
        if row == " Error termination in NtrErr:":
            redflag = True

        if row[0:31] == " Error termination via Lnk1e in":
            redflag = True

        if row[0:15] == " Frequencies --":
            trail = row[15:].lstrip().rstrip().split()

            for freq in trail:
                if not freq == '':
                    frequencies.append(float(freq))

        if row[0:15] == " IR Inten    --":
            trail = row[15:].lstrip().rstrip().split()

            for intens in trail:
                if not intens == '':
                    intensities.append(float(intens))

    if len(frequencies) == 0 or len(intensities) == 0:
        redflag=True

    if not redflag:
        F = np.asarray(frequencies)
        if F.min() < 0:
            print("Warning: imaginairy frequencies found in %s!" %(log_file))
        I = np.asarray(intensities)
        peaksum = np.zeros(shape=(len(xaxis),1))

        #Scaling:
        F = F*scale

        for i in range(0,len(F)):
            xval = F[i]
            yval = I[i]
            for j in range(0,len(xaxis)):
                peaksum[j] += yval*np.exp(-np.square(float(xaxis[j])-float(xval))/(2*sigma*sigma))
        ymax = peaksum.max()
        peaksum[:] = [y / ymax for y in peaksum]
        peaksum = np.transpose(peaksum).round(10).tolist()[0]
    return peaksum

#%% User input
# Convolution options
scale = 0.965 # Scaling factor
FWHM = 12 #cm-1, FWHM of convolution

# Folder and file options
output_dir = os.getcwd() + "/log/"
current_dir = os.getcwd()
log_dir =  output_dir
exp_spectrum ="Experiment.txt"
comparison_file = output_dir + "outputFile.csv" # output file (.csv)

#%% Define some variables
log_list = []
log_dict = {}
scores = []
row1 = "wavenumber, " + exp_spectrum

#%% Open experimental spectrum
f = open(current_dir + "/" + exp_spectrum)
xaxis,yvalues = [], []
for l in f:
    if len(l) > 1:
        row = l.split()
        xaxis.append(round(float(row[0]),5))
        yvalues.append(float(row[1]))

ymax = max(yvalues)
yvalues[:] = [round(y / ymax,10) for y in yvalues]

#%% Open log files and extract their computed IR spectrum and match to exp
for file in os.listdir(log_dir):
    if file.endswith(".log"):
        log_list.append(file)

for log_file in log_list:
    f = open(log_dir + log_file)
    log_dict[log_file] = [readlog(f,xaxis,scale,FWHM)]
    f.close()
    if not log_dict[log_file]:
        del log_dict[log_file]
        print(log_file + " errored")
    else:
        # If log file is ok, calculate matching score
        log_dict[log_file].append(cosine_scoring(yvalues,log_dict[log_file][0]))
        scores.append((log_file,log_dict[log_file][1]))

sorted_scores = sorted(scores, key=lambda tup: tup[1],reverse=True)

#%% Write csv file containing the experimental and computed spectra, including their scores
for log_file,score in sorted_scores:
    row1 = row1 + ", %s (%.4f)" %(log_file[:-4],round(score,4))

f = open(comparison_file,"w")
print(row1)
f.write(row1 + "\n")

for i in range(len(xaxis)):
    row = "%.5f, %.10f" %(xaxis[i],yvalues[i])
    for log_file,score in sorted_scores:
        row = row + ", %.10f" %(log_dict[log_file][0][i])
    f.write(row + "\n")

f.close()
